public class SzukajNaiwnieImpl implements ISzukaj{

    @Override
    public int szukajTekstu(String tekstSzukany, String tekst) {

        for (int i = 0, j = 0; i < tekst.length(); i++) {
            if(tekst.charAt(i) ==  tekstSzukany.charAt(j)) {
                if(j == tekstSzukany.length() - 1 ) {
                    return i-j;
                }
                j++;
            }
        }
        return -1;
    }
}
