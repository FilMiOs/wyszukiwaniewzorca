import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SzukajTest {

    String tekst = "Ala ma kota, a kot ma Ale";
    String tekstSzukany = "kot";
    private static SzukajNaiwnieImpl szukajNaiwnie;

    @BeforeClass
    public static void setupClass() {
        szukajNaiwnie = new SzukajNaiwnieImpl();
    }

    @Test
    public void szukajKotaUAli() {
        Assert.assertEquals(7 , szukajNaiwnie.szukajTekstu(tekstSzukany,tekst));
        Assert.assertEquals(22 , szukajNaiwnie.szukajTekstu("ale",tekst));
    }

    @Test
    public void szukamyTekstuKtoregoNieMa(){
        Assert.assertNotEquals(3, szukajNaiwnie.szukajTekstu("szukaj", tekst));
    }

}
